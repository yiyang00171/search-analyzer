<?php

// composer autoload will be used in real project
include __DIR__ . '/autoload.php';

/**
 * Application and CrawlerFactor only needs one instance globally so Singleton pattern is applied.
 * There could be multiple type of crawlers for example Google, Bing, etc for different search engines.
 * Factory pattern is used to generate crawler base on the search engine.
 */
$crawlerFactory = \SearchAnalyzer\SearchResultCrawler\CrawlerFactory::getInstance();
$crawler = $crawlerFactory->getCrawler('google');

$app = \SearchAnalyzer\Application::getInstance(
    $crawler,
    new \SearchAnalyzer\Analyzer\RankingAnalyzer(),
    new \SearchAnalyzer\ContentParser\SiteUrlParser()
);

$app->run();
