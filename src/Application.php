<?php

namespace SearchAnalyzer;

use SearchAnalyzer\Analyzer\AnalyzerInterface;
use SearchAnalyzer\ContentParser\ContentParserInterface;
use SearchAnalyzer\SearchResultCrawler\CrawlerInterface;
use SearchAnalyzer\SearchResultCrawler\DailyCacheStrategy;
use SearchAnalyzer\SearchResultCrawler\RealTimeStrategy;

final class Application {

    /**
     * Singleton instance
     * @var Application
     */
    private static $instance;

    /**
     * @var CrawlerInterface
     */
    private $crawler;

    /**
     * @var ContentParserInterface
     */
    private $contentParser;

    /**
     * @var AnalyzerInterface
     */
    private $analyzer;

    /**
     * @param CrawlerInterface $crawler
     * @param AnalyzerInterface $analyzer
     * @param ContentParserInterface $contentParser
     * @return Application
     */
    public static function getInstance(CrawlerInterface $crawler, AnalyzerInterface $analyzer, ContentParserInterface $contentParser) {
        if (static::$instance === null) {
            static::$instance = new static($crawler, $analyzer, $contentParser);
        }
        return static::$instance;
    }

    /**
     * @param CrawlerInterface $crawler
     * @param ContentParserInterface $contentParser
     * @param AnalyzerInterface $analyzer
     */
    private function __construct(CrawlerInterface $crawler, AnalyzerInterface $analyzer, ContentParserInterface $contentParser) {
        $this->crawler = $crawler;
        $this->analyzer = $analyzer;
        $this->contentParser = $contentParser;
    }

    public function run() {
        $arguments = $_SERVER['argv'];

        // By default the crawler will reuse cached search result within the same day
        $crawlingStrategy = new DailyCacheStrategy();
        if (count($arguments) < 3 || count($arguments) > 4) {
            throw new \InvalidArgumentException('Please run the app with correct arguments, php ./app.php <keyword> <url> [--real-time]');
        }

        if (count($arguments) === 4) {
            if ($arguments[3] !== '--real-time') {
                throw new \InvalidArgumentException('Please run the app with correct arguments, php ./app.php <keyword> <url> [--real-time]');
            }
            // Cached search result will be ignored when choosing real time strategy
            $crawlingStrategy = new RealTimeStrategy();
        }

        $searchKeyword = $arguments[1];
        $expectedUrl = $arguments[2];
        $this->analyzer->setExpectedUrl($expectedUrl);

        /**
         * Strategy pattern is typically used when there are different types of behavior need to be selected/rotated
         * Crawler can switch between real time and daily cache mode
         */
        $this->crawler->setCrawlingStrategy($crawlingStrategy);
        $rawPages = $this->crawler->crawl($searchKeyword);
        foreach ($rawPages as $rawPage) {
            $parsedPage = $this->contentParser->parseContent($rawPage->getContent(), $rawPage->getPageNumber());
            $this->analyzer->addData($parsedPage);
        }

        $report = $this->analyzer->analyze();
        echo "Rankings: ";
        echo $report . "\n";
    }

}