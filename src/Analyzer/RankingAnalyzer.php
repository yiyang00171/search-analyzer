<?php


namespace SearchAnalyzer\Analyzer;


use SearchAnalyzer\ContentParser\PageInterface;

class RankingAnalyzer implements AnalyzerInterface {

    /**
     * @var PageInterface[]
     */
    private $parsedPages = [];

    /**
     * @var string
     */
    private $expectedUrl;

    /**
     * @param PageInterface $parsedPage
     */
    public function addData(PageInterface $parsedPage) {
        $this->parsedPages[] = $parsedPage;
    }

    /**
     * @return string
     */
    public function getExpectedUrl() {
        return $this->expectedUrl;
    }

    /**
     * @param string $expectedUrl
     */
    public function setExpectedUrl($expectedUrl) {
        $sanitizedExpectedUrl = str_replace('www.', '', $expectedUrl);
        if (strlen($sanitizedExpectedUrl) < 6) {
            throw new \InvalidArgumentException('Excpected URL too short: ' . $expectedUrl);
        }
        $this->expectedUrl = $sanitizedExpectedUrl;
    }

    public function analyze() {
        usort($this->parsedPages, function(PageInterface $prev, PageInterface $next) {
            return ($prev->getPageNumber() < $next->getPageNumber()) ? -1 : 1;
        });

        $results = [];
        foreach ($this->parsedPages as $parsedPage) {
            $ranks = $this->getRanks($parsedPage, $this->expectedUrl);
            if (!$ranks) {
                continue;
            }
            $results += $ranks;
        }

        return implode(', ', $results);
    }

    private function getRanks(PageInterface $parsedPage, $expectedUrl) {
        $ranks = [];
        $urls = explode("\n", $parsedPage->getContent());
        foreach ($urls as $key => $url) {
            if (stripos($url, $expectedUrl) === false) {
                continue;
            }
            $ranks[] = $key + 1 + ($parsedPage->getPageNumber() - 1) * 10;
        }
        return $ranks;
    }

}