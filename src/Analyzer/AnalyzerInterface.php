<?php


namespace SearchAnalyzer\Analyzer;


use SearchAnalyzer\ContentParser\PageInterface;

interface AnalyzerInterface {

    /**
     * @return string
     */
    public function analyze();

    /**
     * @param string $expectedUrl
     */
    public function setExpectedUrl($expectedUrl);

    /**
     * @param PageInterface $data
     */
    public function addData(PageInterface $data);

}