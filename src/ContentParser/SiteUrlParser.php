<?php


namespace SearchAnalyzer\ContentParser;


class SiteUrlParser implements ContentParserInterface {

    public function parseContent($htmlContent, $pageNumber) {
        preg_match_all('/\<div class="BNeawe UPmit \w+"\>([^\< ]+)/', $htmlContent, $matches);
        $parsedContent = isset($matches[1]) ? implode("\n", $matches[1]): null;

        return new WebPage($parsedContent, $pageNumber);
    }

}