<?php


namespace SearchAnalyzer\ContentParser;


interface ContentParserInterface {

    /**
     * @param string $htmlContent
     * @param int $pageNumber
     * @return PageInterface
     */
    public function parseContent($htmlContent, $pageNumber);

}