<?php


namespace SearchAnalyzer\ContentParser;


class WebPage implements PageInterface {

    /**
     * @var int
     */
    private $pageNumber;

    /**
     * @var string
     */
    private $pageContent;

    /**
     * @param string $pageContent
     * @param int $pageNumber
     */
    public function __construct($pageContent, $pageNumber) {
        $this->pageContent = $pageContent;
        $this->pageNumber = (int) $pageNumber;
    }

    /**
     * @return int
     */
    public function getPageNumber() {
        return $this->pageNumber;
    }

    /**
     * @return string
     */
    public function getContent() {
        return $this->pageContent;
    }

}