<?php


namespace SearchAnalyzer\ContentParser;


interface PageInterface {

    /**
     * @return int
     */
    public function getPageNumber();

    /**
     * @return string
     */
    public function getContent();

}