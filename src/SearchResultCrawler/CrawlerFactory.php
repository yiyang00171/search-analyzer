<?php


namespace SearchAnalyzer\SearchResultCrawler;


final class CrawlerFactory {

    /**
     * Singleton instance
     * @var CrawlerFactory
     */
    private static $instance;

    /**
     * @return CrawlerFactory
     */
    public static function getInstance() {
        if (static::$instance === null) {
            static::$instance = new static();
        }
        return static::$instance;
    }

    private function __construct() {

    }

    /**
     * @param string $searchEngine
     * @return CrawlerInterface
     */
    public function getCrawler($searchEngine) {
        $className = explode('\\', get_class($this));
        array_pop($className);
        $className[] = ucfirst($searchEngine) . 'Crawler';
        $className = implode('\\', $className);

        if (!class_exists($className)) {
            throw new \InvalidArgumentException(sprintf('Unsupported search engine: %s', $searchEngine));
        }

        return new $className();
    }

}