<?php


namespace SearchAnalyzer\SearchResultCrawler;


abstract class AbstractCrawler implements CrawlerInterface {

    /**
     * @var int
     */
    protected $maxPageNumber;

    /**
     * @var CrawlingStrategyInterface
     */
    protected $crawlingStrategy;

    /**
     * @param int $maxPageNumber
     * @return $this
     */
    public function setMaxPageNumber($maxPageNumber) {
        $this->maxPageNumber = (int) $maxPageNumber;
        return $this;
    }

    /**
     * @param string $searchKeyword
     * @param int $pageNumber
     * @return string
     */
    abstract protected function buildSearchUrl($searchKeyword, $pageNumber);

    /**
     * @return string
     */
    abstract protected function getSearchEngineName();

    /**
     * @param string $searchKeyword
     * @param int $pageNumber
     * @return string
     */
    protected function crawlSinglePage($searchKeyword, $pageNumber) {
        $cacheFilePath = $this->getCacheFilePath($searchKeyword, $pageNumber);
        if ($this->isCacheValid($cacheFilePath)) {
            return file_get_contents($cacheFilePath);
        }

        // A simple naive crawler
        $url = $this->buildSearchUrl($searchKeyword, $pageNumber);
        return file_get_contents($url);
    }

    /**
     * @param CrawlingStrategyInterface $crawlingStrategy
     * @return $this
     */
    public function setCrawlingStrategy(CrawlingStrategyInterface $crawlingStrategy) {
        $this->crawlingStrategy = $crawlingStrategy;
        return $this;
    }

    protected function getCacheFilePath($searchKeyword, $pageNumber) {
        $fileName = sprintf(
            '%s-%s-%s-%d.html',
            $this->getSearchEngineName(),
            (new \DateTime())->format('Y-m-d'),
            $searchKeyword,
            $pageNumber
        );

        return __DIR__ . '/../../cache/' . $fileName;
    }

    protected function cache($searchKeyword, $pageNumber, $pageContent) {
        file_put_contents($this->getCacheFilePath($searchKeyword, $pageNumber), $pageContent);
    }

    public function getIntervalBetweenPages() {
        return $this->crawlingStrategy->getIntervalBetweenPages();
    }

    public function isCacheValid($cacheFilePath) {
        return $this->crawlingStrategy->isCacheValid($cacheFilePath);
    }

}