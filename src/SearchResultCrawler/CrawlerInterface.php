<?php


namespace SearchAnalyzer\SearchResultCrawler;


use SearchAnalyzer\ContentParser\PageInterface;

interface CrawlerInterface extends CrawlingStrategyInterface {

    /**
     * @param string $searchKeyword
     * @return PageInterface[]
     */
    public function crawl($searchKeyword);

    /**
     * @param int $maxPageNumber
     */
    public function setMaxPageNumber($maxPageNumber);

    /**
     * @param CrawlingStrategyInterface $strategy
     * @return CrawlerInterface
     */
    public function setCrawlingStrategy(CrawlingStrategyInterface $strategy);

}