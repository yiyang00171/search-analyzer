<?php


namespace SearchAnalyzer\SearchResultCrawler;


use SearchAnalyzer\ContentParser\PageInterface;
use SearchAnalyzer\ContentParser\WebPage;

class GoogleCrawler extends AbstractCrawler {

    protected function getSearchEngineName() {
        return 'google';
    }

    /**
     * @param string $searchKeyword
     * @return PageInterface[]
     */
    public function crawl($searchKeyword) {
        $pages = [];
        $searchKeyword = $this->preProcessKeyword($searchKeyword);
        for ($pageNumber = 1; $pageNumber <= 10; $pageNumber++) {
            $pageContent = $this->crawlSinglePage($searchKeyword, $pageNumber);

            $pages[] = new WebPage($pageContent, $pageNumber);
            $this->cache($searchKeyword, $pageNumber, $pageContent);
        }

        return $pages;
    }

    protected function preProcessKeyword($searchKeyword) {
        return str_replace(' ', '+', $searchKeyword);
    }

    protected function buildSearchUrl($searchKeyword, $pageNumber) {
        return sprintf('https://www.google.com.au/search?q=%s&start=%d', $searchKeyword, ($pageNumber - 1) * 10);
    }

}