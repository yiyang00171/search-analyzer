<?php


namespace SearchAnalyzer\SearchResultCrawler;


class DailyCacheStrategy implements CrawlingStrategyInterface {

    public function getIntervalBetweenPages() {
        return 1;
    }

    public function isCacheValid($cacheFilePath) {
        /**
         * Cache file pattern is like google-2019-07-04-search-keyword-1.html
         * Cached result will be returned to avoid real time search.
         */
        return file_exists($cacheFilePath);
    }

}