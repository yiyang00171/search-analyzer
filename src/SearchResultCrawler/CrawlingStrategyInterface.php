<?php


namespace SearchAnalyzer\SearchResultCrawler;


interface CrawlingStrategyInterface {

    /**
     * seconds
     * @return int
     */
    public function getIntervalBetweenPages();

    /**
     * @param string $cacheFilePath
     * @return bool
     */
    public function isCacheValid($cacheFilePath);

}