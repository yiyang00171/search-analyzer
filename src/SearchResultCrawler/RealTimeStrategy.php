<?php


namespace SearchAnalyzer\SearchResultCrawler;


class RealTimeStrategy implements CrawlingStrategyInterface {

    public function getIntervalBetweenPages() {
        return 0;
    }

    public function isCacheValid($cacheFilePath) {
        // Never uses cache. Always crawl real time search result.
        return false;
    }

}