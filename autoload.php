<?php

// PSR-4
spl_autoload_register(function ($className) {
    // e.g. SearchAnalyzer\SearchResultCrawler\GoogleCrawler
    $namespaceParts = explode('\\', $className);
    $partsCount = count($namespaceParts);
    if ($partsCount < 2) {
        throw new RuntimeException(sprintf('Namespace missing in class name: %s', $className));
    }

    $sourceFilePrefix = 'src/';
    $filePath = '';

    foreach ($namespaceParts as $index => $namespacePart) {
        // Skip first part 'SearchAnalyzer'
        if ($index == 0) {
            continue;
        }

        if ($index == $partsCount - 1) {
            $filePath .= $namespacePart . '.php';
            continue;
        }

        $filePath .= $namespacePart . '/';
    }

    include_once $sourceFilePrefix . $filePath;

});

