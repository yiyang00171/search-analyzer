# search-analyzer

A prototype of SEO ranking analyzer

This app is a prototype of SEO ranking analyzer.
For a given keyword and expected URL, the app will print out a list of ranking numbers
for where the expected URL is found in the Google results.

To use the app, firstly clone the repository to your local machine.
git clone git@bitbucket.org:yiyang00171/search-analyzer.git

In the root folder, run the command:

php ./app.php <keyword> <url>

e.g.
php ./app.php creditorwatch creditorwatch.com.au
php ./app.php "credit bureau" creditorwatch.com.au


By default, search result will be cached and reused for one day when search for the same keyword.
Cached result will be saved as html files in cache folder.

Design pattern used: Singleton, Factory, Strategy. Please find details in app.php and Application.php.